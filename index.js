//class cart
class Cart {
  #_contents = []
  #_totalAmount = 0

  //1st arg accepts instance of product only
  //2nd accepts number only for quantity
  addToCart(product, quantity) {
    if ((product instanceof Product) & (typeof quantity === 'number')) {
      this.#_contents.push({ product, quantity })
      this.#computeTotal()
    } else {
      console.log(false)
    }
    return this
  }

  //private method to compute total amount
  #computeTotal() {
    this.#_totalAmount = this.#_contents
      .map((item) => item.product.getPrice() * item.quantity)
      .reduce((acc, curr) => acc + curr, 0)
    return this
  }

  //method to show cart contents
  showCartContents() {
    // console.log(this.#_contents)
    // return this

    return this.#_contents
  }

  //get cart amount
  getCartTotalAmount() {
    return this.#_totalAmount
  }

  //method to update product quantity
  updateProductQuantity(name, quantity) {
    if (typeof quantity === 'number') {
      this.#_contents.forEach((item) => {
        if (item.product.getName() === name) {
          item.quantity = quantity
        }
      })
      this.#computeTotal()
    }
    return this
  }

  //method to clear cart
  clearCartContents() {
    this.#_contents = []
    this.#_totalAmount = 0
    return this
  }
}

//class customer
class Customer {
  #_orders = []
  #_cart = new Cart()
  #_email

  constructor(email) {
    this.#_email = email
  }

  cart() {
    return this.#_cart
  }

  getOrders() {
    console.log(this.#_orders)
    return this
  }

  getEmail() {
    return this.#_email
  }

  viewCart() {
    console.log(this.#_cart)
    return this
  }

  //to follow: group product per name
  checkout() {
    // const { content, total } = this.cart()
    this.#_orders.push({
      products: this.cart().showCartContents(),
      totalAmount: this.cart().getCartTotalAmount(),
    })
    return this
    // this.#_orders.push(this.#_cart)
  }
}
//testing class customer
const john = new Customer('john@mail.com')
console.log(john)

//class product
class Product {
  #_name
  #_price
  #_isActive = true

  constructor(name, price) {
    this.#_name = name
    this.#_price = price
  }

  //get private fields
  getPrice() {
    return this.#_price
  }

  getName() {
    return this.#_name
  }

  getStatus() {
    return this.#_isActive
  }

  //archive product
  archive() {
    this.#_isActive = false
    return this
  }

  //unarchive product
  activate() {
    this.#_isActive = true
    return this
  }

  //rename product
  rename(name) {
    this.#_name = name
    return this
  }

  //update product price
  udpatePrice(price) {
    this.#_price = price
    return this
  }
}
//testing class Product
const prodA = new Product('soap', 9.99)
const prodB = new Product('shampoo', 5)
console.log('prodA before archive', prodA)
prodA.archive()
console.log('prodA after archive', prodA)
prodA.activate()
console.log('prodA after activate', prodA)

john.cart().addToCart(prodA, 3)
john.cart().addToCart(prodA, 3)
john.cart().addToCart(prodB, 10)
john.viewCart()
john.cart().showCartContents()
john.cart().updateProductQuantity('soap', 1)
console.log('after qty update')
john.cart().showCartContents()
john.viewCart()
// john.cart().clearCartContents()
john.checkout()
john.getOrders()
